#!/bin/bash

################################################################################
# The MIT License (MIT)                                                        #
#                                                                              #
# Copyright (c) 2016 Alexander Saupp, Achim Christ                             #
#                                                                              #
# Permission is hereby granted, free of charge, to any person obtaining a copy #
# of this software and associated documentation files (the "Software"), to deal#
# in the Software without restriction, including without limitation the rights #
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    #
# copies of the Software, and to permit persons to whom the Software is        #
# furnished to do so, subject to the following conditions:                     #
#                                                                              #
# The above copyright notice and this permission notice shall be included in   #
# all copies or substantial portions of the Software.                          #
#                                                                              #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  #
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,#
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE#
# SOFTWARE.                                                                    #
################################################################################

################################################################################
# Name:         Check IBM Spectrum Scale / GPFS
# Author:       Alexander Saupp - asaupp(at)gmail(dot)com
# Author:       Achim Christ - achim(dot)christ(at)gmail(dot)com
# Contributor:  Christian Fey
# Contributor:  Nils Haustein
# Version:      1.2
# Dependencies:
#               - IBM Spectrum Scale
#               - bc (a numeric processing language)
################################################################################

# This bash script checks various aspects of an IBM Spectrum Scale (a.k.a GPFS)
# cluster. It verifies the node state, filesystem mount state, capacity and
# inode utilization, and physical disk state of IBM Elastic Storage Server (ESS)
# systems. Typically, it would be run on all nodes in a cluster via NRPE.

# There are various plugins available already which intend to provide similar
# functionality, but this script is actively maintained, well documented, and
# built on reusable components to ease future extension.

# The actual code is managed in the following Git rebository - please use the
# Issue Tracker to ask questions, report problems or request enhancements. The
# repository also contains an extensive README.
#   https://gitlab.com/itsmee/icinga

# Disclaimer: This sample is provided 'as is', without any warranty or support.
# It is provided solely for demonstrative purposes - the end user must test and
# modify this sample to suit his or her particular environment. This code is
# provided for your convenience, only - though being tested, there's no
# guarantee that it doesn't seriously break things in your environment! If you
# decide to run it, you do so on your own risk!

################################################################################
## Installation
################################################################################

# Run the checks provided by this script on all nodes in your cluster. Certain
# checks will only actually do something useful on the cluster manager to save
# resources, but will return OK on all other nodes.

# Typically you'll want to use NRPE for running the script. Don't forget to
# enable COMMAND ARGUMENT PROCESSING (dont_blame_nrpe=1) and COMMAND PREFIX
# (command_prefix=/usr/bin/sudo) when doing so.

# Here's a sample NRPE configuration snippet:
#   ...
#   command[check_spectrumscale_state]=/usr/lib64/nagios/plugins/check_spectrumscale.sh -s
#   command[check_spectrumscale_mount]=/usr/lib64/nagios/plugins/check_spectrumscale.sh -m $ARG1$
#   command[check_spectrumscale_health]=/usr/lib64/nagios/plugins/check_spectrumscale.sh -e
#   command[check_spectrumscale_disk]=/usr/lib64/nagios/plugins/check_spectrumscale.sh -d $ARG1$
#   command[check_spectrumscale_capacity]=/usr/lib64/nagios/plugins/check_spectrumscale.sh -c $ARG1$ -w 75
#   command[check_spectrumscale_inodes]=/usr/lib64/nagios/plugins/check_spectrumscale.sh -i $ARG1$ -w 75
#   command[check_spectrumscale_pdisk]=/usr/lib64/nagios/plugins/check_spectrumscale.sh -p
#   command[check_spectrumscale_gnrhealth]=/usr/lib64/nagios/plugins/check_spectrumscale.sh -g
#   ...


################################################################################
## Future topics
################################################################################
# - Check mount state / capacity / inodes for ALL mounted file systems
# - Quota reporting
# - Check for waiters
# - AFM DR (work in progress)
# - ?

# Ignore linter errors
# shellcheck disable=SC1117

# Treat unset variables as errors
set -u


################################################################################
## Variable definition
################################################################################

SCALE_PATH="/usr/lpp/mmfs/bin/"
HOSTNAME=$( hostname | sed "s/\..*$//" )  ## E.g. mmlsmount shows a list of hosts. This hostname is used to filter the line of the
                                          ## local host. Usually that should work - if your hostname is not part of your gpfs node name
                                          ## you need to patch this


################################################################################
## Constant definition
################################################################################
readonly RC_OK=0
readonly RC_WARNING=1
readonly RC_CRITICAL=2
readonly RC_UNKNOWN=3


################################################################################
## Check Args
##
## Ensure valid paramters are given
################################################################################

error_usage () {
  ERROR=$1
  HELP="
    Check IBM Spectrum Scale / GPFS status (MIT licence)

    usage: $0 [ -s | -m <fs> | -d <fs> | -c <fs> -w <% free> | -i <fs> -w <% free> | -h | -p | -g ]

    syntax:
        -s                  --> Verify IBM Spectrum Scale status
        -m <fs>             --> Verify <fs> mount status
        -e                  --> Verify 'mmhealth node show' status
        -d <fs>             --> Verify disk state for <fs>
        -c <fs> -w <% free> --> Verify <fs> capacity and inodes
        -i <fs> -w <% free> --> Verify inode utilization for <fs>
        -p                  --> Verify IBM ESS status (pdisk)
        -g                  --> Verify IBM ESS status (gnrhealth)
        -a <fs>             --> Verify IBM Spectrum Scale AFM Cache State
        -h                  --> Print This Help Screen
  "

  OLD_IFS="$IFS"
  IFS=""
  echo -e "$HELP"
  IFS="$OLD_IFS"

  if [ "$ERROR" == "" ] ; then
    echo -e "\nYou'll probably execute this script via NRPE remotely"
  else
    echo -e "$0 $*"
    echo -e "\n$ERROR"
  fi
  exit $RC_UNKNOWN
}

CHECK=""

while getopts 'sm:ed:c:i:w:a:pgh' OPT ; do
  case $OPT in
    s)  CHECK="s";;
    m)  CHECK="m"; filesystem=$OPTARG;;
    e)  CHECK="e";;
    d)  CHECK="d"; filesystem=$OPTARG;;
    c)  CHECK="c"; filesystem=$OPTARG;;
    i)  CHECK="i"; filesystem=$OPTARG;;
    a)  CHECK="a"; filesystem=$OPTARG;;
    w)  THRESHOLD=$OPTARG;;
    p)  CHECK="p";;
    g)  CHECK="g";;
    h)  error_usage;;
    *)  error_usage "Unknown parameter \"$OPT\"";;
  esac
done

check_ok=0
error_msg="Wrong parameter(s)"

# Check if running as root
if [ "$(whoami)" != "root" ] ; then
  echo "Must be run as root"
  exit $RC_UNKNOWN
fi

# Check number of commandline options
if [ $# -eq 0 ] ; then
   check_ok=0
elif [ "$CHECK" == "s" ] && [ $# -eq 1 ] ; then
  check_ok=1
elif [ "$CHECK" == "m" ] && [ $# -eq 2  ] && [ "$filesystem" != "" ] ; then
  check_ok=1
elif [ "$CHECK" == "e" ] && [ $# -eq 1 ] ; then
  if [ ! -f "/usr/lpp/mmfs/bin/mmhealth" ] ; then
    echo "Cannot find /usr/lpp/mmfs/bin/mmhealth"
    exit $RC_UNKNOWN
  else
    check_ok=1
  fi
elif [ "$CHECK" == "d" ] && [ $# -eq 2  ] && [ "$filesystem" != "" ] ; then
  check_ok=1
elif [ "$CHECK" == "c" ] && [ $# -eq 4  ] && [ "$filesystem" != "" ] && [ "$THRESHOLD" != "" ] ; then
  check_ok=1
elif [ "$CHECK" == "i" ] && [ $# -eq 4  ] && [ "$filesystem" != "" ] && [ "$THRESHOLD" != "" ] ; then
  check_ok=1
elif [ "$CHECK" == "p" ] && [ $# -eq 1  ] ; then
  if [ ! -f "/usr/lpp/mmfs/bin/mmlspdisk" ] ; then
    echo "Cannot find /usr/lpp/mmfs/bin/mmlspdisk"
    exit $RC_UNKNOWN
  else
    check_ok=1
  fi
elif [ "$CHECK" == "a" ] && [ $# -eq 2 ] && [ "$filesystem" != "" ] ; then
  check_ok=1
elif [ "$CHECK" == "g" ] && [ $# -eq 1  ] ; then
  if [ ! -f "/usr/lpp/mmfs/bin/gnrhealthcheck" ] ; then
    echo "Cannot find /usr/lpp/mmfs/bin/gnrhealthcheck"
    exit $RC_UNKNOWN
  else
    check_ok=1
  fi
fi

if [ "$check_ok" == 0 ] ; then
  error_usage "$error_msg"
fi


################################################################################
## Check prereqs
##
## gpfs installed? bc installed?
################################################################################
if [ ! -d /usr/lpp/mmfs/bin/ ] || [ ! -f /usr/lpp/mmfs/bin/mmgetstate ] ; then
  echo "No IBM Spectrum Scale Installation found"
  exit $RC_UNKNOWN
fi

if [ "$CHECK" == "c" ] || [ "$CHECK" == "i" ] ; then
  if ! which bc &>/dev/null; then
    echo "UNKNOWN: Require 'bc' tool for this check"
    exit $RC_UNKNOWN
  fi
fi


################################################################################
## Helper functions
##
## Those two functions will be used to parse the ":" separated output that most
## IBM Spectrum Scale commands can return if called with "-Y"
## Make use of them - no need to understand
################################################################################

################################################################################
## getColumnNoByHeader
##
## Helper to parse ":" separated IBM Spectrum Scale output
## Will return columnNr for given Header Name - case insensitiv header compare
##
## Expects: ArrayName containing ouput, header name
## Return: ColumnNr
################################################################################
function getColumnNoByHeader( ) {
  arrName="$1[@]"                       ## First arg is an array name that was dereference here
  lines=("${!arrName}")                 ## Array contains all lines of our command return
  header="${2,,}"                       ## Second arg is the header we're looking for - convert to all lowercases

  headerLine="${lines[0],,}"            ## First line is header - convert to all lower cases

  declare -A lookup
  IFS=":"
  count="0"

  for column in $headerLine ; do
    if [ "$column" != "" ] ; then
      lookup[$column]="$count"
    fi
    count=$((count+1))
  done

  IFS=$'\n'

  return ${lookup[$header]}
}

############################################################
## getColumnValueByColHeader
##
## Helper to parse ":" separated IBM Spectrum Scale output
## Will return column content (1st line with data, if multiple) for given Header Name
##
## Expects: ArrayName containing ouput, header name
## Return: column content
############################################################
function getColumnValueByColHeader() {
  arrName="$1[@]"                       ## First arg is an array name that we derference here
  lines=("${!arrName}")                 ## Array contains all lines of our command return
  header="$2"                           ## Second arg is the header we're looking for
  row="${3:-1}"                         ## Optional third arg - defaults to 1

  IFS=":"
  columns=(${lines[$row]})
  IFS=$'\n'

  getColumnNoByHeader "lines" "$header"
  col=$?

  echo "${columns[$col]}"
  return 0
}


################################################################################
## Main
##
## This is where the checks start - for debug / additional checks, start
## reading here
################################################################################


################################################################################
## Check IBM Spectrum Scale status
##
## Will verify that "mmgetstate" on local node is active
################################################################################
## Sample output - this is what we're going to parse
#mmgetstate::HEADER:version:reserved:reserved:nodeName:nodeNumber:state:quorum:nodesUp:totalNodes:remarks:cnfsState:
#mmgetstate::0:1:::g1_node1:1:active:2:2:2:quorum node:(undefined):

if [ "$CHECK" == "s" ] ; then

  cmd="mmgetstate -Y"         ## Define command
  out=$( ${SCALE_PATH}$cmd )  ## Run & collect
  IFS=$'\n';                  ## Define line separator (re-apply default)
  cmd_return=( $out )         ## Split output into array - one per line

  gpfsState=$( getColumnValueByColHeader cmd_return "state" ) ## Get column 'state' from second row (1st = header)

  if [ "$gpfsState" == "active" ] ; then
    echo "OK: GPFS state is $gpfsState"
    exit $RC_OK
  elif [ "$gpfsState" == "arbitrating" ] ; then
    echo "WARNING: GPFS state is $gpfsState"
    exit $RC_WARNING
  else
    echo "ERROR: GPFS state is $gpfsState"
    exit $RC_CRITICAL
  fi
fi


################################################################################
## Check IBM Spectrum Scale mount state
##
## Verify mount state of given filesystem on local node
################################################################################
## Sample output - this is what we're going to parse
#mmlsmount::HEADER:version:reserved:reserved:localDevName:realDevName:owningCluster:totalNodes:nodeIP:nodeName:clusterName:mountMode:
#mmlsmount::0:1:::group1fs:group1fs:g1_cluster.local:2:192.168.0.112:g1_node2:g1_cluster.local:RW:
#mmlsmount::0:1:::group1fs:group1fs:g1_cluster.local:2:192.168.0.111:g1_node1:g1_cluster.local:RW:

if [ "$CHECK" == "m" ] ; then

  cmd="mmlsmount $filesystem -L -Y"                      ## Define command
  out=$( ${SCALE_PATH}$cmd | grep "HEADER\|$HOSTNAME" )  ## Run & collect - make sure 'our' node is in line 2 via grep
                                                         ## MISSING: grep for hostname - might not be our GPFS node name?
  IFS=$'\n';                                             ## Define line separator (re-apply default)
  cmd_return=( $out )                                    ## Split output into array - one per line

  # Check if file system exists
  if [ -z "${cmd_return-}" ] ; then
    echo "UNKNOWN: File system $filesystem not known"
    exit $RC_UNKNOWN
  fi

  mountedTotal=$( getColumnValueByColHeader cmd_return "totalNodes" ) ## Get column value from line 2
  mountedOn=$( getColumnValueByColHeader cmd_return "nodeName" )      ## Get column value from line 2

  if [ "$mountedOn" != "" ] ; then
    echo "OK: File system $filesystem is mounted on $HOSTNAME and on $mountedTotal nodes in total"
    exit $RC_OK
  else  ## mmlsmount didn't show our hostname
    echo "ERROR: File system $filesystem doesn't seem to be mounted on $HOSTNAME"
    exit $RC_CRITICAL
  fi
fi


################################################################################
## Check IBM Spectrum Scale mmhealth state
##
## Verify local node health with mmhealth node show
################################################################################
## Sample output - this is what we're going to parse
#mmhealth:State:HEADER:version:reserved:reserved:node:component:entityname:entitytype:status:laststatuschange:
#mmhealth:Event:HEADER:version:reserved:reserved:node:component:entityname:entitytype:event:arguments:activesince:identifier:
#mmhealth:State:0:1:::ems1-bond0.spectrum:NODE:ems1-bond0.spectrum:NODE:HEALTHY:2017-03-30 12%3A09%3A19.996226 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:GUI:ems1-bond0.spectrum:NODE:HEALTHY:2017-03-23 16%3A02%3A35.344167 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:GPFS:ems1-bond0.spectrum:NODE:HEALTHY:2017-03-30 12%3A09%3A19.991371 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:NETWORK:ems1-bond0.spectrum:NODE:HEALTHY:2017-03-06 10%3A55%3A57.758440 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:NETWORK:enP6p112s0:NODE:HEALTHY:2017-03-06 10%3A55%3A57.758547 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:NETWORK:bond0:NODE:HEALTHY:2017-03-06 10%3A55%3A57.758644 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:NETWORK:enP6p112s0d1:NODE:HEALTHY:2017-03-06 10%3A55%3A57.758738 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:PERFMON:ems1-bond0.spectrum:NODE:HEALTHY:2017-03-06 10%3A55%3A57.899551 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:FILESYSTEM:ems1-bond0.spectrum:NODE:HEALTHY:2017-03-30 12%3A09%3A19.992118 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:FILESYSTEM:hanashared:FILESYSTEM:HEALTHY:2017-03-06 10%3A59%3A59.886238 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:FILESYSTEM:gpfs0:FILESYSTEM:HEALTHY:2017-03-06 10%3A59%3A59.886322 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:FILESYSTEM:hanadata:FILESYSTEM:HEALTHY:2017-03-06 10%3A59%3A59.886403 CEST:
#mmhealth:State:0:1:::ems1-bond0.spectrum:FILESYSTEM:hanalog:FILESYSTEM:HEALTHY:2017-03-06 10%3A59%3A59.886484 CEST:

if [ "$CHECK" == "e" ] ; then

  ## Get overall node state
  cmd="mmhealth node show -Y"                           ## Define command
  out=$( ${SCALE_PATH}$cmd | grep -e State | head -2 )  ## Run & collect - make sure 'our' node is in line 2 via grep
                                                        ## MISSING: grep for hostname - might not be our GPFS node name?
  IFS=$'\n';                                            ## Define line separator (re-apply default)
  cmd_return=( $out )                                   ## Split output into array - one per line

  RESULT_STATE=$( getColumnValueByColHeader cmd_return "status" ) ## Get column value from line 2

  if [ "$RESULT_STATE" != "HEALTHY" ] ; then
    ## Get all 'not healthy' states
    IFS=$' '
    cmd="mmhealth node show -Y"                 ## Define command
    out=$( ${SCALE_PATH}$cmd | grep -e Event )  ## Run & collect
    IFS=$'\n';                                  ## Define line separator (re-apply default)
    cmd_return=( $out )                         ## Split output into array - one per line

    RESULT=""
    for line in "${!cmd_return[@]}" ; do  ## Loop through all lines - each should be for a separate fileset
      if [ "$line" -gt "0" ] ; then       ## Skip header row
        RESULT="$RESULT|"$( getColumnValueByColHeader cmd_return "event" "$line" )
      fi
    done
  fi

  ## Return one overall status - containing "\n" separated detail lines from all checks
  if [ "$RESULT_STATE" == "HEALTHY" ] ; then
    echo -e "OK: mmhealth"
    exit $RC_OK
  else
    echo -e "$RESULT_STATE: $RESULT"
    exit $RC_WARNING
  fi
fi


################################################################################
## Check fs capacity & inodes
##
## use mmdf to verify capacity of
##   - (single) filesystem given
##   - all pools contained (one pool might be 20%, one 100%)
##   - inodes of root fileset
##
## Might still see out-of-space conditions if independent filsets within fs
## run out of inodes. There is a separate check for that below.
##
## Note: this check only actually runs on the cluster manager (to be lighter on
## resources). All other nodes will return OK.
################################################################################
## Sample output - this is what we're going to parse
#mmdf:nsd:HEADER:version:reserved:reserved:nsdName:storagePool:diskSize:failureGroup:metadata:data:freeBlocks:freeBlocksPct:freeFragments:freeFragmentsPct:diskAvailableForAlloc:
#mmdf:poolTotal:HEADER:version:reserved:reserved:poolName:poolSize:freeBlocks:freeBlocksPct:freeFragments:freeFragmentsPct:maxDiskSize:
#mmdf:fsTotal:HEADER:version:reserved:reserved:fsSize:freeBlocks:freeBlocksPct:freeFragments:freeFragmentsPct:
#mmdf:inode:HEADER:version:reserved:reserved:usedInodes:freeInodes:allocatedInodes:maxInodes:
#mmdf:nsd:0:1:::nsd4:system:10485760:1:Yes:Yes:8749056:83:4032:0::
#mmdf:nsd:0:1:::nsd3:system:10485760:1:Yes:Yes:8717312:83:1920:0::
#mmdf:nsd:0:1:::nsd2:system:10485760:2:Yes:Yes:8734720:83:6432:0::
#mmdf:nsd:0:1:::nsd1:system:10485760:2:Yes:Yes:8726528:83:4640:0::
#mmdf:poolTotal:0:1:::system:41943040:34927616:83:17024:0:1227025408:
#mmdf:fsTotal:0:1:::41943040:34927616:83:17024:0:
#mmdf:inode:0:1:::4126:198882:203008:203008:

if [ "$CHECK" == "c" ] ; then

  # Only run on cluster manager to save resources
  LOCALNODEFQDN=$(${SCALE_PATH}mmlsnode -N localhost)
  LOCALNODE=`echo $LOCALNODEFQDN |awk -F'.' '{print $1}'`
  CLUSTERMGR=$(${SCALE_PATH}mmlsmgr -c | sed 's|.*(||' | sed 's|)||')

  if [ "$LOCALNODE" != "$CLUSTERMGR" ] ; then
    echo "OK: Check skipped - will execute on cluster mgr only"
    exit $RC_OK
  fi

  # Running on cluster manager... continue

  cmd="mmdf $filesystem -Y"   ## Define command
  out=$( ${SCALE_PATH}$cmd )  ## Run & collect
  IFS=$'\n';                  ## Define line separator (re-apply default)
  cmd_return=( $out )         ## Split output into array - one per line

  # Check if file system exists
  if [ -z "${cmd_return-}" ] ; then
    echo "UNKNOWN: File system $filesystem not known"
    exit $RC_UNKNOWN
  fi

  RESULT_STATE="OK"  ## Assume green until we find otherwise
  RESULT=""
  for line in "${!cmd_return[@]}" ; do ## Loop through all lines looking for particular ones:
                                       ## poolTotal, fsTotal, inode summary lines - see if statements below

    nsdName=$( getColumnValueByColHeader cmd_return "nsdName" "$line" ) ## Get column values from the current line
                                                                        ## TRICKY - there are three different header lines - depending on line type the 1st line header doesn't match content
    nsd=$( getColumnValueByColHeader cmd_return "nsd" "$line" )
    storagePool=$( getColumnValueByColHeader cmd_return "storagePool" "$line" )
    diskSize=$( getColumnValueByColHeader cmd_return "diskSize" "$line" )
    failureGroup=$( getColumnValueByColHeader cmd_return "failureGroup" "$line" )

    ## Check pool capacity
    if [ "$nsd" == "poolTotal" ] && [ "$nsdName" != "poolName" ] ; then
      # echo "$nsd: $nsdName|$storagePool|$diskSize|$failureGroup"  ## debugging only - keep as comment
      if [ "$failureGroup" -le "$THRESHOLD" ] ; then
        RESULT_STATE="WARNING"
        RESULT="$RESULT Pool $nsdName: $failureGroup% free, should be: $THRESHOLD% / "
      else
        RESULT="$RESULT Pool $nsdName: $failureGroup% free / "
      fi

    ## Check fs capacity
    elif [ "$nsd" == "fsTotal" ] && [ "$nsdName" != "fsSize" ] ; then
      # echo "$nsd: $nsdName|$storagePool|$diskSize|$failureGroup"  ## debugging only - keep as comment
      if [ "$diskSize" -le "$THRESHOLD" ] ; then
        RESULT_STATE="WARNING"
        RESULT=$RESULT"fs $filesystem: $diskSize% free, should be: $THRESHOLD% / "
      else
        RESULT=$RESULT"fs $filesystem: $diskSize% free / "
      fi

    ## Check fs inodes
    ## Calculate % free via bc tool
    elif [ "$nsd" == "inode" ] && [ "$nsdName" != "usedInodes" ] ; then
      # echo "$nsd: $nsdName|$storagePool|$diskSize|$failureGroup"  ## debugging only - keep as comment
      inode_free_percent=$(echo "100-($nsdName/$storagePool)*100" | bc -l | sed "s/\.[0-9]*$//" )

      if [ "$inode_free_percent" == "" ] ; then
        RESULT_STATE="WARNING"
        RESULT=$RESULT"Free inode percentage calculation failed"
      elif [ "$inode_free_percent" -le "$THRESHOLD" ] ; then
        RESULT_STATE="WARNING"
        RESULT=$RESULT"fs $filesystem: $inode_free_percent% free inodes ($nsdName/$storagePool), should be: $THRESHOLD%"
      else
        RESULT=$RESULT"fs $filesystem: $inode_free_percent% free inodes ($nsdName/$storagePool)"
      fi
    fi
  done

  ## Return one overall status - containing "\n" separated detail lines from all checks
  if [ "$RESULT_STATE" == "OK" ] ; then
    echo -e "$RESULT_STATE: $RESULT"
    exit $RC_OK
  else
    echo -e "$RESULT_STATE: $RESULT"
    exit $RC_WARNING
  fi
fi


################################################################################
## Check IBM Spectrum Scale inode utilization
##
## Check free inodes for all fileset for a given fs
## Root fileset is checked via filesystem and fileset (no harm)
##
## Note: this check only actually runs on the cluster manager (to be lighter on
## resources). All other nodes will return OK.
################################################################################
## Sample output - this is what we're going to parse
#mmlsfileset::HEADER:version:reserved:reserved:filesystemName:filesetName:id:rootInode:status:path:parentId:created:inodes:dataInKB:comment:filesetMode:afmTarget:afmState:afmMode:afmFileLookupRefreshInterval:afmFileOpenRefreshInterval:afmDirLookupRefreshInterval:afmDirOpenRefreshInterval:afmAsyncDelay:afmNeedsRecovery:afmExpirationTimeout:afmRPO:afmLastPSnapId:inodeSpace:isInodeSpaceOwner:maxInodes:allocInodes:inodeSpaceMask:afmShowHomeSnapshots:afmNumReadThreads:reserved:afmReadBufferSize:afmWriteBufferSize:afmReadSparseThreshold:afmParallelReadChunkSize:afmParallelReadThreshold:snapId:afmNumFlushThreads:afmPrefetchThreshold:afmEnableAutoEviction:permChangeFlag:afmParallelWriteThreshold:freeInodes:afmNeedsResync:afmParallelWriteChunkSize:afmNumWriteThreads:afmPrimaryID:afmDRState:afmAssociatedPrimaryId
#mmlsfileset::0:1:::group1fs:root:0:3:Linked:%2Fibm%2Fgroup1fs:--:Fri Dec  4 16%3A08%3A19 2015:-:-:root fileset:off:-:-:-:-:-:-:-:-:-:-:-:-:0:1:102912:102912:512:-:-:-:-:-:-:-:-:0:-:-:-:chmodAndSetacl:-:98802:-:-:-:-:-:-:
#mmlsfileset::0:1:::group1fs:worm:1:131075:Linked:%2Fibm%2Fgroup1fs%2Fworm:0:Sun Dec 20 16%3A39%3A48 2015:-:-::noncompliant:-:-:-:-:-:-:-:-:-:-:-:-:1:1:100096:100096:512:-:-:-:-:-:-:-:-:0:-:-:-:chmodAndSetacl:-:100080:-:-:-:-:-:-:

if [ "$CHECK" == "i" ] ; then

  # Only run on cluster manager to save resources
  LOCALNODEFQDN=$(${SCALE_PATH}mmlsnode -N localhost)
  LOCALNODE=`echo $LOCALNODEFQDN |awk -F'.' '{print $1}'`
  CLUSTERMGR=$(${SCALE_PATH}mmlsmgr -c | sed 's|.*(||' | sed 's|)||')

  if [ "$LOCALNODE" != "$CLUSTERMGR" ] ; then
    echo "OK: Check skipped - will execute on cluster mgr only"
    exit $RC_OK
  fi

  # Running on cluster manager... continue

  cmd="mmlsfileset $filesystem -Y"  ## Define command
  out=$( ${SCALE_PATH}$cmd )        ## Run & collect
  IFS=$'\n';                        ## Define line separator (re-apply default)
  cmd_return=( $out )               ## Split output into array - one per line

  # Check if file system exists
  if [ -z "${cmd_return-}" ] ; then
    echo "UNKNOWN: File system $filesystem not known"
    exit $RC_UNKNOWN
  fi

  RESULT_STATE="OK"  ## Assume green until we find otherwise
  RESULT=""
  for line in "${!cmd_return[@]}" ; do  ## Loop through all lines - each should be for a separate fileset
    if [ "$line" -gt "0" ] ; then       ## Skip header row
      filesetName=$( getColumnValueByColHeader cmd_return "filesetName" "$line" )
      freeInodes=$( getColumnValueByColHeader cmd_return "freeInodes" "$line" )
      maxInodes=$( getColumnValueByColHeader cmd_return "maxInodes" "$line" )

      if [ "$maxInodes" -gt "0" ] ; then  ## Make sure it's an independent fileset with dedicated inode space
        inode_free_percent=$(echo "($freeInodes/$maxInodes)*100" | bc -l | sed "s/\.[0-9]*$//" ) ## Calculate free inode percentage
        if [ "$inode_free_percent" == "" ] ; then
          RESULT_STATE="WARNING"
          RESULT=$RESULT"Free inode percentage calculation failed"
        elif [ "$inode_free_percent" -le "$THRESHOLD" ] ; then  ## Enough inodes available?
          RESULT_STATE="WARNING"
          RESULT=$RESULT"$filesystem/$filesetName: $inode_free_percent% free inodes ($freeInodes/$maxInodes), should be: $THRESHOLD% / "
        else
          RESULT=$RESULT"$filesystem/$filesetName: $inode_free_percent% free inodes ($freeInodes/$maxInodes) / "
        fi
      else  ## maxInodes=0 for dependent filesets, so cannot do calculation with this
          RESULT=$RESULT"$filesystem/$filesetName: dependent fileset, uses inodes from parent fileset / "
      fi
    fi
  done

  ## Return one overall status - containing "\n" separated detail lines from filesets
  if [ "$RESULT_STATE" == "OK" ] ; then
    echo -e "$RESULT_STATE: $RESULT"
    exit $RC_OK
  else
    echo -e "$RESULT_STATE: $RESULT"
    exit $RC_WARNING
  fi
fi


################################################################################
## Check IBM ESS pdisks
##
## mmlspdisk --not-ok should return header only, if more than one line
## returned, this means there is administrative attention required.
################################################################################
## Sample output - this is what we're going to parse
#mmlspdisk:pdisk:HEADER:version:reserved:reserved:replacementPriority:pdiskName:paths:recoveryGroup:declusteredArray:state:capacity:freeSpace:fru:location:WWN:server:reads:writes:bytesReadInGiB:bytesWrittenInGiB:IOErrors:IOTimeouts:mediaErrors:checksumErrors:pathErrors:relativePerformance:dataBadness:rgIndex:userLocation:userCondition:vendor:product:revision:serial:hardwareType:nPathsActive:nPathsTotal:expectedPathsActive:expectedPathsTotal:nPathsActiveWrong:nPathsTotalWrong:

if [ "$CHECK" == "p" ] ; then

  cmd="mmlspdisk all --not-ok -Y"  ## Define command
  out=$( ${SCALE_PATH}$cmd )       ## Run & collect
  IFS=$'\n';                       ## Define line separator (re-apply default)
  cmd_return=( $out )              ## Split output into array - one per line

  if [ "${#cmd_return[@]}" -gt "1" ] ; then  ## mmlspdisk found not-ok disks
    error="WARNING: Found not-ok pdisks"
    for line in "${!cmd_return[@]}" ; do  ## Loop through all lines - each should be for a separate fileset
      if [ "$line" -gt "0" ] ; then       ## Skip header row
        pdiskName=$( getColumnValueByColHeader cmd_return "pdiskName" "$line" )
        state=$( getColumnValueByColHeader cmd_return "state" "$line" )
        error="$error $pdiskName=$state |"
      fi
    done
    echo "$error"
    exit $RC_WARNING
 else
   echo "OK: mmlspdisk"
   exit $RC_OK
 fi
fi


################################################################################
## Check IBM ESS gnrhealth
##
## Run gnrhealthcheck & verify return code
################################################################################
## No sample output - we'll just return and not parse

if [ "$CHECK" == "g" ] ; then

  ## gnrhealthcheck
  cmd="gnrhealthcheck 2>/dev/null"         ## Define command
  out=$( ${SCALE_PATH}$cmd | tail -n 10 )  ## No parsable output - limit length
  returnCode=$?                            ## Keep track of return code

  if [ "$returnCode" -ne "0" ] ; then  ## gnrhealthcheck returnCode requires attention
    echo -e "ERROR: gnrhealthcheck has return code $returnCode\n$out"
    exit $RC_CRITICAL
  else
    echo "OK: gnrhealthcheck"
    exit $RC_OK
  fi
fi


################################################################################
## Check IBM Spectrum Scale disk state
##
## Verify disk state of given filesystem
##
## Note: this check only actually runs on the cluster manager (to be lighter on
## resources). All other nodes will return OK.
################################################################################
## Sample output - this is what we're going to parse
#mmlsdisk::HEADER:version:reserved:reserved:nsdName:driverType:sectorSize:failureGroup:metadata:data:status:availability:diskID:storagePool:remarks:numQuorumDisks:readQuorumValue:writeQuorumValue:diskSizeKB:diskUID:
#mmlsdisk::0:1:::nsd1:nsd:512:1:Yes:Yes:ready:up:1:system:desc:2:2:2:10485760:C0A8008258072D39:
#mmlsdisk::0:1:::nsd2:nsd:512:1:Yes:Yes:ready:up:2:system:desc:2:2:2:10485760:C0A8008258072D37:

if [ "$CHECK" == "d" ] ; then

  # Only run on cluster manager to save resources
  LOCALNODEFQDN=$(${SCALE_PATH}mmlsnode -N localhost)
  LOCALNODE=`echo $LOCALNODEFQDN |awk -F'.' '{print $1}'`
  CLUSTERMGR=$(${SCALE_PATH}mmlsmgr -c | sed 's|.*(||' | sed 's|)||')

  if [ "$LOCALNODE" != "$CLUSTERMGR" ] ; then
    echo "OK: Check skipped - will execute on cluster mgr only"
    exit $RC_OK
  fi

  # Running on cluster manager... continue

  cmd="mmlsdisk $filesystem -Y"   ## Define command
  out=$( ${SCALE_PATH}$cmd )      ## Run & collect - make sure 'our' node is in line 2 via grep
                                  ## MISSING: grep for hostname - might not be our GPFS node name?
  IFS=$'\n';                      ## Define line separator (re-apply default)
  cmd_return=( $out )             ## Split output into array - one per line

  # Check if file system exists
  if [ -z "${cmd_return-}" ] ; then
    echo "UNKNOWN: File system $filesystem not known"
    exit $RC_UNKNOWN
  fi

  RESULT_STATE="OK"  ## Assume green until we find otherwise
  RESULT=""
  for line in "${!cmd_return[@]}" ; do  ## Loop through all lines - each should be for a separate fileset
    if [ "$line" -gt "0" ] ; then       ## Skip header row
      nsdName=$( getColumnValueByColHeader cmd_return "nsdName" "$line" )    ## Get column value nsdName
      status=$( getColumnValueByColHeader cmd_return "status" "$line" )      ## Get column value status
      avail=$( getColumnValueByColHeader cmd_return "availability" "$line" ) ## Get column value availability

      if [ "$status" != "ready" ] ; then  ## check the disk status against "ready"
        RESULT_STATE="WARNING"
        RESULT=$RESULT"Disk $nsdName status is $status /"
      elif [ "$avail" != "up" ] ; then  ## check the disk availability against "up"
        RESULT_STATE="WARNING"
        RESULT=$RESULT"Disk $nsdName availability is $avail /"
      else
        RESULT=$RESULT"Disk $nsdName is OK / "
      fi
    fi
  done

  ## Return one overall status - containing "\n" separated detail lines from filesets
  if [ "$RESULT_STATE" == "OK" ] ; then
    echo -e "$RESULT_STATE: $RESULT"
    exit $RC_OK
  else
    echo -e "$RESULT_STATE: $RESULT"
    exit $RC_WARNING
  fi
fi


################################################################################
## Check IBM Spectrum Scale afm-dr state
##
## Verify afm-dr status of a given filesystem
################################################################################
## Sample output - this is what we're going to parse
#mmafmctl:getState:HEADER:version:reserved:reserved:filesystemName:filesetName:afmTarget:cacheState:gatewayNode:queueLength:numExec:DRState:
#mmafmctl:getState:0:1:::gwork1:import:nfs%3A//mapping1/ibm/gwork1/import:Active:ess1pn3-hs:0:528317:Active:

if [ "$CHECK" == "a" ] ; then

  cmd="mmafmctl $filesystem getstate -Y"  ## Define command
  out=$( ${SCALE_PATH}$cmd )              ## Run & collect - make sure 'our' node is in line 2 via grep
  IFS=$'\n';                              ## Define line separator (re-apply default)
  cmd_return=( $out )                     ## Split output into array - one per line

  # Check if file system exists
  if [ -z "${cmd_return-}" ] ; then
    echo "UNKNOWN: File system $filesystem not known"
    exit $RC_UNKNOWN
  fi

  ## Check if we have AFM filesets in that filesystem
  if ${SCALE_PATH}mmafmctl "$filesystem" getstate -Y | grep -i "No valid filesets" &>/dev/null; then
    echo "WARNING: No valid AFM filesets for $filesystem"
    exit $RC_WARNING
  fi

  RESULT_STATE="CANTHAPPEN"  ## Assume green until we find otherwise
  RESULT=""
  FAILUREDETECTED=0
  for line in "${!cmd_return[@]}" ; do  ## Loop through all lines - each should be for a separate fileset
    if [ "$line" -gt "0" ] ; then       ## Skip header row
      filesetName=$( getColumnValueByColHeader cmd_return "filesetName" "$line" )
      cacheState=$( getColumnValueByColHeader cmd_return "cacheState" "$line" )
      #DRState=$( getColumnValueByColHeader cmd_return "DRState" "$line" )
      #queueLength=$( getColumnValueByColHeader cmd_return "queueLength" "$line" )

      if [ "$cacheState" == "Disconnected" ] ; then
        RESULT_STATE="WARNING"
        RESULT=$RESULT"MDS cannot connect to the NFS server at home for fileset $filesetName"
        FAILUREDETECTED=1
      elif [ "$cacheState" == "Unmounted" ] ; then
        RESULT_STATE="WARNING"
        RESULT=$RESULT"Home NFS is not accessible for fileset $filesetName"
        FAILUREDETECTED=1
      elif [ "$cacheState" == "Dropped" ] ; then
        RESULT_STATE="WARNING"
        RESULT=$RESULT"AFM State for fileset $filesetName is dropped"
        FAILUREDETECTED=1
      elif [ "$cacheState" == "Active" ] ; then
        RESULT_STATE="OK"
        RESULT=$RESULT"$filesetName=active:"
      elif [ "$cacheState" == "Dirty" ] ; then
        RESULT_STATE="OK"
        RESULT=$RESULT"There are pending pending changes in cache that are not yet played at home for fileset $filesetName"
      else
        RESULT_STATE="WARNING"
        RESULT=$RESULT"The fileset state is unknown"
        FAILUREDETECTED=1
      fi
    fi
    if [ "$FAILUREDETECTED" -gt "0" ] ; then
      RESULT_STATE="WARNING"
      RESULT=$RESULT
   fi
  done

  ## Return one overall status - containing "\n" separated detail lines from filesets
  if [ "$RESULT_STATE" == "OK" ] ; then
    echo -e "$RESULT_STATE: $RESULT"
    exit $RC_OK
  else
    echo -e "$RESULT_STATE: $RESULT"
    exit $RC_WARNING
  fi
fi


## We'll never get beyond this point
## All states must be handled above & 'exit RETCODE' called

echo "ERROR: Run away code - you should never see this"
exit $RC_UNKNOWN
